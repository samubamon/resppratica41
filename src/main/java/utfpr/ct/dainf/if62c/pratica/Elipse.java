/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author samu
 */
public class Elipse {
    private double eixoR;
    private double eixoS;
    
    public Elipse(double semiR, double semiS){
        this.eixoR = 2*semiR;
        this.eixoS = 2*semiS;
    }

    public double getEixoR() {
        return eixoR;
    }

    public void setEixoR(double eixoR) {
        this.eixoR = eixoR;
    }

    public double getEixoS() {
        return eixoS;
    }

    public void setEixoS(double eixoS) {
        this.eixoS = eixoS;
    }
    
    
    public double getArea(){
       eixoR = eixoR/2;
       eixoS = eixoS/2;
        
       return Math.PI*eixoR*eixoS;
    }
    
   public double getPerimetro(){
       eixoR = eixoR/2;
       eixoS = eixoS/2;
       
       return Math.PI*(3*(eixoR + eixoS) - Math.sqrt((3*eixoR + eixoS)*(eixoR + 3*eixoS)));
    }
}
